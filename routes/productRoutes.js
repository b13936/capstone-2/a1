
const express = require("express");
const router = express.Router();
const productController = require("./../controllers/productControllers");
const auth = require("./../auth");


// All Courses
router.get("/", (req, res) => {
	productController.getProducts().then( result => res.send(result))
})


// Create Product
router.post("/create-product", auth.verify,(req, res) => {

	let data = {
		admin: auth.decode(req.headers.authorization).isAdmin,
		body: req.body
	}

	productController.createCourse(data).then(result => res.send(result));
}) 

// Active Courses
router.get("/active-products", (req, res) => {
	productController.getActiveProducts().then( result => res.send(result))
})


// Specific Course
router.get("/:productId", (req,res) => {
	productController.getSpecificProduct(req.params).then( result => res.send(result))
})

// Edit Product
router.put("/:productId/edit", auth.verify, (req, res) => {

	let data = {
		admin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productId,
		body: req.body
	}

	productController.editProduct(data).then( result => res.send(result))
})


// Archive Order
router.put("/archive", auth.verify,(req,res) => {

	let data = {
		admin: auth.decode(req.headers.authorization).isAdmin,
		productName: req.body.productName
	}

	productController.archiveProducts(data).then( result => res.send(result))
})

router.put("/unarchive", auth.verify,(req,res) => {

	let data = {
		admin: auth.decode(req.headers.authorization).isAdmin,
		productName: req.body.productName
	}

	productController.unarchiveProducts(data).then( result => res.send(result))
})




module.exports = router;