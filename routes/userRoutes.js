

const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers");
const auth = require("./../auth");


// User Registration
router.post("/register", (req,res) => {

	userController.register(req.body).then( result => res.send(result))
});


// Login
router.post("/login", (req,res) => {

	userController.login(req.body).then(result => res.send(result))
});

// Make Admin
router.put("/make-admin", auth.verify,(req,res) => {

	let data = {
		admin: auth.decode(req.headers.authorization).isAdmin,
		email: req.body.email
	}

	userController.makeAdmin(data).then( result => res.send(result))
})

// Get users
router.get("/", (req,res) => {

	userController.getAllUsers().then( result => res.send(result))
})

// Create Order
router.post("/order", auth.verify,(req,res) => {
     
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userController.order(data).then(result => res.send(result))
})

router.post("/remove-order", auth.verify,(req,res) => {
     
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userController.removeOrder(data).then(result => res.send(result))
})

// Get Order
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
		
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Authenticated user's orders

router.post("/cart", auth.verify,(req, res) => {

	let user = {
		userId: auth.decode(req.headers.authorization).id
	}

	userController.getCart(user).then(result => res.send(result))
})



router.get("/all-orders", auth.verify,(req, res) => {

	let user = {
		userId: auth.decode(req.headers.authorization)
	}

	userController.getAllOrders(user).then(result => res.send(result))
})




module.exports = router;