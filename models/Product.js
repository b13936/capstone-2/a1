
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	
	{
		productName:{
			type: String,
			required: [true, "Name is required"]
		},

		description:{
			type: String,
			required: [true, "Description is required"]
		},

		price:{
			type: Number,
			required: [true, "Product price "]
		},

		isActive: {
			type: Boolean,
			default: true
		},

		createdOn: {
			type: Date,
			default: new Date()
		},

	}
);


module.exports = mongoose.model("Product", productSchema)