
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(

	{
		email:{
			type: String,
			required: [true, "Email is required"]
		},
		password:{
			type: String,
			required: [true, "Password is required"]
		},
		isAdmin:{
			type: Boolean,
			default: false
		},
		// mobileNo : {
		// 	type : String, 
		// 	required : [true, "Mobile No is required"]
		// },
		orders:[{
			productId:{
				type: String,
				required:[true,"Product ID is required"]
			},
			productName:{
				type: String,
				required:[true,"Product Name is required"]
			},
			description:{
				type: String,
				required:[true,"Description is required"]
			},
			price:{
				type: Number,
				required:[true,"Price is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Ordered"
			}
		}]
	}
);


module.exports = mongoose.model("User", userSchema)