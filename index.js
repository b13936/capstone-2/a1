// Dependencise and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

// Environment Variables Setup
dotenv.config();
const secret = process.env.CONNECTION_STRINGl;


// Server Setup
const app = express();
const PORT = process.env.PORT || 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());




mongoose.connect('mongodb+srv://khrade:Kronos12@batch139.lgkts.mongodb.net/capstone2?retryWrites=true&w=majority', secret,
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true
	}
);


// Database Connect
const db = mongoose.connection;
	db.on("error",() => console.error(`Connection failed`));
	db.once("open", () => console.log(`Connected to Database`));




// Server
app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);


// Server Response
app.listen(PORT, () => console.log(`Server running at port ${PORT}`))

