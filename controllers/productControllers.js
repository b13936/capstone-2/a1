
const Product = require("./../models/Product");


// Get Active Products
module.exports.getProducts = () => {

	return Product.find().then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

// Create Product
module.exports.createCourse = (data) => {

	const{admin, body} = data

	const{productName, description, price} = body

	let newProduct = new Product({
		productName: productName,
		description: description,
		price: price

	})

	return Product.findOne(body).then( (result, error) => {

		if(result == null && admin == true){

			return newProduct.save(body).then( (res, err) => {
				if (err) {
					return err
				} else {
					return res
				}
			})
		} else {
			if (result == null){
				return `Not an Admin`
			} else {
				return `Course Already Exists`
			}
		}
	})
}


// Get Active Products
module.exports.getActiveProducts = () => {

	return Product.find({isActive:true}).then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

// Specific Products
module.exports.getSpecificProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then( (result,error) => {

		if (result == null) {
			return 'Product not Existing'
		} else {
			
			if (result){
				return result
			} else {
				return error
			}
		}
	})
}


// Update Product
module.exports.editProduct = (data) => {

	// console.log(data)

	const{admin, productId, body} = data

	const {productName, description, price, isActive} = body

	let updatedProduct = {
		productName: productName,
		description: description,
		price: price,
		isActive: isActive
	}

	return Product.findById(productId).then( (result, error) => {

/*			console.log(data)

			console.log(admin)*/

			if(admin == true){
				return Product.findByIdAndUpdate(productId, updatedProduct, {new: true}).then( (res, err) => {
						if (err) {
							return err
						} else {
							return res
						}
				})
			} else {

				if(result == null){
					return `Product does not exist`
				} else {
					return `Not an Admin`
				}
			}
		}) 
}


// Archive Products
module.exports.archiveProducts = (data) => {

	const{admin, productName} = data

	let userStatus = {
		isActive: false
	}

	return Product.findOne({productName}).then( (result,error) => {

		if (admin == true) {

			return Product.findOneAndUpdate({productName}, userStatus, {new: true}).then( (res,err) => {

					if (err) {
							return false
						} else {
							return true
						}
			})
		} 
	})
}


// Unarchive Products
module.exports.unarchiveProducts = (data) => {

	const{admin, productName} = data

	let userStatus = {
		isActive: true
	}

	return Product.findOne({productName}).then( (result,error) => {

		if (admin == true) {

			return Product.findOneAndUpdate({productName}, userStatus, {new: true}).then( (res,err) => {

					if (err) {
							return false
						} else {
							return true
						}
			})
		} 
	})
}