
const User = require('./../models/User');
const bcrypt = require("bcrypt");
const auth = require("./../auth");
const Product = require('./../models/Product')


// User Registration


module.exports.register = (reqBody) => { 

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),  
		isAdmin: reqBody.isAdmin
	})

	return User.findOne({email: reqBody.email}).then( (result,error) => {
		if(result != null){
			return `Email already exists`
		} else {
			return newUser.save(reqBody).then( (resolve, err) => {
				if(err){
					return false
				} else {
					return true
				}
			})
		}
	})
}


// User authentication
module.exports.login = (reqBody) => {

	const {email, password} = reqBody;

	return User.findOne({email: email}).then( (result,error) => {

		if(result == null){
			return false
		} else {
			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}

		}
	})
}


// Make Admin
module.exports.makeAdmin = (data) => {

	const {admin, email} = data

	let userStatus = {
		isAdmin: true
	}

	// console.log(data.email)

	return User.findOneAndUpdate({email}, userStatus, {new: true}).then( (result,error) => {

		if (admin != true) {

			Status = {
				isAdmin: false
			}

			return User.findOneAndUpdate({email}, Status, {new: true}).then( (result,error) => {

				return 'Authenticated user is not an admin'
				
			})

		} else {
			return result
		}	
	})
}



// get users
module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {
		if(result){
			return result
		} else {
			return error
		}
	})
}

// Order
module.exports.order = (data) => {

	const {userId, productId} = data
	// console.log(data)
	

	return User.findById(userId).then( (customer, err) => {

		if(err){ 

			return err

		} else {
			// console.log(result)

			Product.findById(productId).then(product => {

				// return result
				if (product == null) {

					return 'Product not Existing'
					
				} else {	

					const {_id, productName, description, price} = product
					// console.log(description)
					// console.log(price)

					customer.orders.push({productId:productId, productName: productName, description: description, price: price})

					customer.save().then( (result, err) => 
							{
								// console.log(result)
							})
						// console.log(customer)

				}


			})

		return customer.orders
			
		}
	})
}

// Remove Order
module.exports.removeOrder = (data) => {

	const {userId, productId, productName, description, price} = data


	return User.findById(userId).then( (customer, err) => {

		if(err){ 

			return err

		} else {
			// console.log(result)

			Product.findById(productId).then(product => {

				// return result
				if (product == null) {

					return 'Product not Existing'
					
				} else {	

					// console.log(product)
					// console.log(customer)
				
					for(let i=0; i < customer.orders.length; i++){
						if (productId == customer.orders[i].productId){
							customer.orders.splice([i],1)
							customer.save().then( (result, err) => 
							{
								// console.log(result)
							})
						}
					}
				
				}

			})
		
		}
		return customer.orders
	})
}




module.exports.asdf = async (data) => {

	// console.log(data)

		if(data.isAdmin === true){

			 return false

		} else {

			let isUserUpdated = await User.findById(data.userId).then(user => {

				user.orders.push({productId : data.productId})
				
				return user.save().then((user, err) => {

					if(err){

						return false

					} else {

						return true
					}
				})
			})

			let isProductUpdated = await Product.findById(data.productId).then(result => {

				return result.orders.save().then((course, err) => {

					if(err){

						 return false

					} else {

						return true
					}
				})
			})

			if(isUserUpdated && isProductUpdated){

				return true

			} else {

				return false
			}
		}
}
/*
	const productOrder = await Product.findById(productId).then( (result, err) => {
		if(err){
			return err
		} else {
			// console.log(result)
			result.enrollees.push({userId: userId})

			if (result.enrollees.userId != userId){

				return result.save().then( (result, err) => {
					if(err){
						return err
					} else {
						return true
					}
				})
			} else {

				return false

			}
		}
	})*/
	
// Get Details

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};


// Get Cart
module.exports.getCart = (data) => {
	const {userId} = data
		// console.log(data)

	return User.findById(userId).then( (result, error) => {

		// console.log(result)

		return result.orders

	})
}



// All Orders
module.exports.getAllOrders = (data) => {

	const {userId} = data

	/*	console.log(data)
		console.log(userId.isAdmin)*/

	return User.find().then( (result, error) => {

		if(userId.isAdmin != true){
			return `Not an Admin`
		} else {
			return result
		}
	})
}





